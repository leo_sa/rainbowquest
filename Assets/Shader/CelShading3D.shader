﻿Shader "Custom/CelShading3D"
{
	Properties
	{		
		[MaterialToggle] HasRimLight("Has Rim Light", Float) = 0
		_Color("Color", Color) = (1,1,1,1) 

		_OutlineColor("Outlien Color", Color) = (1,1,1,1)
		_OutlineWidth("Outline Width", Range(0,0.1)) = 0.03

        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _NormalMap ("NormalMap", 2D) = "white" {}
		_CellLut ("Cell LUT", 2D) = "white" {}
		_RimPower("Rim Power", Range(0.5, 8)) = 3

		_ShadowBrightness("Shadow Brightness", Range(0, 0.69)) = 0.3
		_HighlightBrightness("Highlight Brightness", Range(0, 0.3)) = 0.1
    }
    SubShader
    {
		Tags { "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM

		#pragma surface surf MyLight 

		#pragma multi_compile DUMMY HASHIGHLIGHT_ON

		#pragma multi_compile DUMMY HASRIMLIGHT_ON


		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _NormalMap;
		sampler2D _CellLut;

		float _HighlightBrightness;
		float _ShadowBrightness;
		fixed4 _Color;

		half4 LightingMyLight(SurfaceOutput o, half3 lightDir, half3 viewDir, half atten) {

			half ndotl = dot(o.Normal, lightDir);		
			half4 c;	

#if defined(_CellLut)
			c.rgb = (o.Albedo * tex2D(_CellLut, float2(ndotl * 0.5f + 0.5, 0)) * _LightColor0.rgb) * (atten);
#else
			if(ndotl > 0.95)
				c.rgb = float4(0.7 + _HighlightBrightness, 0.7 + _HighlightBrightness, 0.7 + _HighlightBrightness, 1) * _LightColor0.rgb * atten * o.Albedo;
			else if (ndotl > 0.5)
				c.rgb = float4(0.7, 0.7, 0.7, 1) * _LightColor0.rgb * atten * o.Albedo;
			else if(ndotl > 0.05)
				c.rgb = float4(0 + _ShadowBrightness, 0 + _ShadowBrightness, 0 + _ShadowBrightness, 1) * _LightColor0.rgb * atten * o.Albedo;
#endif

			c.a = o.Alpha;
			return c;

		}

		struct Input
		{
			float2 uv_MainTex;
			fixed4 color;
			float3 viewDir;
		};
		

		half _Glossiness;
		half _Metallic;
		float _RimPower;

		UNITY_INSTANCING_BUFFER_START(Props)

		UNITY_INSTANCING_BUFFER_END(Props)

		void surf(Input IN, inout SurfaceOutput o)
		{
			
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

			#if defined (_NormalMap)
			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
			#endif

			o.Albedo = c.rgb * _Color;

#if defined(HASRIMLIGHT_ON)
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			o.Albedo += (pow(rim, _RimPower) * 3)  * _LightColor0.rgb;
#endif

			o.Alpha = c.a;
		}
		ENDCG

			Pass{

				Cull Front

				CGPROGRAM

				#pragma vertex V2F
				#pragma fragment Frag

				half _OutlineWidth;

				float4 V2F(float4 position : POSITION, float3 normal : NORMAL) : SV_POSITION{

					float4 clipPosition = UnityObjectToClipPos(position);
					float3 clipNormal = mul((float3x3) UNITY_MATRIX_VP, mul((float3x3) UNITY_MATRIX_M, normal));

					clipPosition.xyz += normalize(clipNormal) * _OutlineWidth;

					float2 offset = normalize(clipNormal.xy) / _ScreenParams.xy * _OutlineWidth * clipPosition.w * 2;
					clipPosition.xy += offset;

					return clipPosition;
				}

				half4 _OutlineColor;

				half4 Frag() : SV_TARGET{
					return _OutlineColor;
				}
					ENDCG
		}
    }
    FallBack "Diffuse"
}
