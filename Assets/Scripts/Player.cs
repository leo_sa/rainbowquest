﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour2
{
    private Rigidbody2D rigidBody2D;

    [SerializeField] private Animator playerAnimator;

    [SerializeField] private float speed;
    [SerializeField] private float jumpForce;
    private float extraForceX, extraForceY;
    private float originalGravity;
    private float originalSpeed;

    public bool canInteract;  

    private void Awake()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        originalGravity = rigidBody2D.gravityScale;
        originalSpeed = speed;
    }


    private void Update()
    {
        ExtraForceDecelleration();
        AnimatorCheck();
        if (canInteract)
        {
            Jump(jumpForce);
            Sprint();
        }
    }

    private void FixedUpdate()
    {
        if (canInteract)
        {
            Acceleretion();
            RayChecks();
        }
    }

    // CORE MOVEMENT //


    private float maxAccelerationTimer = 0.1f;
    private float accelerationTimer = 0.1f;
    private float accelerationSpeed = 0.16f;

    private float accelerationHorizontal;

    private int horizontalRaw;

    private void Acceleretion()
    {
        horizontalRaw = (int)Input.GetAxisRaw("Horizontal");   

        if(horizontalRaw != 0 && accelerationTimer >= 0)
        {
           if(Mathf.Abs(horizontalRaw * 1.2f - accelerationHorizontal) > 0.1f  && !(Mathf.Abs(accelerationHorizontal) > Mathf.Abs(horizontalRaw * 1.2f))) accelerationHorizontal += (accelerationSpeed * horizontalRaw) * TimeMultiplication();
           accelerationTimer += Time.deltaTime;
        }
        else
        {
            if (accelerationHorizontal != 0) accelerationHorizontal = SmoothLerpValue(accelerationHorizontal, 0, 0.1f, 0.25f);
            accelerationTimer = maxAccelerationTimer;
        }
        CoreMovement(accelerationHorizontal);
    }

    private bool wasSprinting;

    private void Sprint()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            speed = Mathf.Lerp(speed, originalSpeed * 1.5f, 0.2f * TimeMultiplication());
            wasSprinting = true;
        }
        else if (wasSprinting)
        {
            if (!Input.GetKey(KeyCode.LeftShift))
            {
                if(Mathf.Abs(originalSpeed- speed) > 0.1f )speed = Mathf.Lerp(speed, originalSpeed, 0.2f * TimeMultiplication());
                else
                {
                    speed = originalSpeed;
                    wasSprinting = false;
                }
            }
        }
    }



    private void CoreMovement(float localSpeed)
    {
        if (!isJumping)
        {
            float secondLocalSpeed = localSpeed;

            if (touchingWall)         // "CustomClamp"
            {
                if (secondLocalSpeed != 0)
                {
                    if (touchingWallLeft)
                    {
                        if (secondLocalSpeed < 0) secondLocalSpeed = 0;
                    }
                    else if (touchingWallRight)
                    {
                        if (secondLocalSpeed > 0) secondLocalSpeed = 0;
                    }
                }
            }
            //MAIN ACCELERATION AND MOVEMENT//

            rigidBody2D.velocity = new Vector2(secondLocalSpeed * Time.deltaTime * speed + extraForceX, rigidBody2D.velocity.y + extraForceY * Time.deltaTime * 100);

        }
    }



    private void ExtraForceDecelleration()
    {
        if (extraForceY != 0) extraForceY = Mathf.Lerp(extraForceY, 0, Time.deltaTime * 60);
        if (extraForceX != 0) extraForceX = Mathf.Lerp(extraForceX, 0, Time.deltaTime * 30);
    }

    //JUMPING//



    private bool isJumping;

    private float maxJumpTimer = 0.15f;
    private float jumpTimer = 0.15f;

    private int jumpCounter = 1;

    private void Jump(float strength)
    {

        if (Input.GetKeyDown(KeyCode.Space) && jumpCounter > 0)
        {
            if (rigidBody2D.gravityScale != originalGravity) rigidBody2D.gravityScale = originalGravity;
            isJumping = true;
            rigidBody2D.velocity += Vector2.up * strength;
            jumpCounter--;
        }
        else if(Input.GetKey(KeyCode.Space) && isJumping)
        {
            if (jumpTimer > 0)
            {
                rigidBody2D.velocity += Vector2.up * strength/30;
                jumpTimer -= Time.deltaTime;
            }
        }
        else if (Input.GetKeyUp(KeyCode.Space))
        {
            if (!grounded) jumpTimer = 0;
        }
    }

    private void UnJump()
    {
        if (isJumping) isJumping = false;
        jumpTimer = maxJumpTimer;
    }



    //CHECKS//

    private float boxHitX, boxHitY; 
    [SerializeField, Range(0, 10)]
    private float rayLength = 4;
    private RaycastHit2D groundHit, leftHit, rightHit;
  
    [SerializeField]
    private LayerMask layerMask;

    private bool grounded;

    private void RayChecks()            
    {
        GroundRayCheck();
        LRRayChecks();
    }

    private void GroundRayCheck()
    {
        groundHit = Physics2D.Raycast(transform.position, Vector2.down, rayLength, layerMask);

        if (groundHit.collider != null)
        {
            grounded = true;
        }
        else
        {
            grounded = false;
        }
    }


    private int wallDirection;

    private bool touchingWallLeft, touchingWallRight, touchingWall;
    private bool leftHitBox, rightHitBox;

    private void LRRayChecks()
    {
        leftHitBox = Physics2D.OverlapBox(new Vector2(transform.position.x - 1.8f, transform.position.y), new Vector2(1f, 2.5f), 0, layerMask);
        rightHitBox = Physics2D.OverlapBox(new Vector2(transform.position.x + 1.8f, transform.position.y), new Vector2(1f, 2.5f), 0, layerMask);


        if (leftHitBox)
        {
            touchingWallLeft = true;
            touchingWall = true;
            wallDirection = -1;
        }
        else if (leftHitBox)
        {
            touchingWallRight = true;
            touchingWall = true;
            wallDirection = 1;
        }
        else
        {
            touchingWallLeft = false;
            touchingWallRight = false;
            touchingWall = false;
            wallDirection = 0;
        }

        Debug.Log(touchingWall);
    }


    //ANIMATOR //

    private bool isMoving;
    private float movingSpeed;

    private void AnimatorCheck()
    {
        if(playerAnimator != null) 
        {
            if (Input.GetAxisRaw("Horizontal") != 0)
            {
                isMoving = true;
            }
            else
            {
                isMoving = false;
            }

            movingSpeed = rigidBody2D.velocity.x;

            AnimatorSet();
        }

    }

    private void AnimatorSet()
    {
        playerAnimator.SetBool("isMoving", isMoving);
        if (isMoving)
        {
            playerAnimator.SetFloat("speed", movingSpeed);
        }
        else
        {
            if(playerAnimator.GetFloat("speed") != 0) playerAnimator.SetFloat("speed", 0);
        }
    }
 


    // NON MOVEMENT METHODS //    

    public void TurnIntangiable(bool enable)
    {
        canInteract = enable;      
    }

        //STD METHODS


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isJumping)
        {
            accelerationTimer /= 2;
            accelerationHorizontal /= 2;
        }     
        UnJump();
        jumpCounter = 1;
    }
  


    //GIZMO//

    private void OnDrawGizmos()
    {
     //  Gizmos.color = new Color(1, 0, 0, 1);
     // Gizmos.DrawCube(new Vector3(transform.position.x - 1f, transform.position.y, 0), new Vector3(1f, 2.5f, 0));
     //  Gizmos.DrawCube(new Vector3(transform.position.x + 1f, transform.position.y, 0), new Vector3(1f, 2.5f, 0));
    }
}

