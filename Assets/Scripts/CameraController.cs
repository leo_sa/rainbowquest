﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour2
{
    [SerializeField] Transform toFollow;
    private Vector3 followVector;

    [SerializeField, Range(0, 1)] private float smoothness;

    private void Start()
    {
        followVector = new Vector3(toFollow.position.x, toFollow.position.y, transform.position.z);
    }

    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, toFollow.position, smoothness * TimeMultiplication());
    }
}
