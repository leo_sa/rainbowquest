﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Staticvalues
{
    public static Camera mainCamera;

    public static bool isPaused;

    [RuntimeInitializeOnLoadMethod]
    public static void InitializeVariables()
    {
        mainCamera = Camera.main;
    }
}
