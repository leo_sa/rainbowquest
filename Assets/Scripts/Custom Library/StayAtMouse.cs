﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayAtMouse : MonoBehaviour
{
    private void FixedUpdate()
    {
        Vector3 cachedFollowVector = Staticvalues.mainCamera.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(cachedFollowVector.x ,cachedFollowVector.y, transform.position.z);
    }
}
